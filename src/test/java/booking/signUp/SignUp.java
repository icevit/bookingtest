package booking.signUp;

import booking.WebDriverSettings;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.Random;

public class SignUp extends WebDriverSettings {

    @Test
    public void singUp() {
        HomePage homePage = PageFactory.initElements(driver, HomePage.class);

        homePage.openReg();

        Random random = new Random();
        int n = random.nextInt(999) + 1;

        String email = "testforbooking" + n + "@yandex.ru";

        SignUpPage signUpPage = PageFactory.initElements(driver, SignUpPage.class);

        signUpPage.inputEmail(email);
        signUpPage.inputPassword("12345432");
        signUpPage.inputPasswordConfirm("12345432");
        signUpPage.submitForm();
        signUpPage.checkSignUpSuccess();
    }

    @Test
    public void singUpFailure() {
        HomePage homePage = PageFactory.initElements(driver, HomePage.class);

        homePage.openReg();

        SignUpPage signUpPage = PageFactory.initElements(driver, SignUpPage.class);

        signUpPage.submitForm();

        signUpPage.checkErrorRequiredField();

        String emailAddressError = signUpPage.getErrorEmail();
        Assert.assertEquals(emailAddressError, "Enter your email address");
    }

    @Test
    public void searchForm() {

        HomePage homePage = PageFactory.initElements(driver, HomePage.class);

        homePage.open();

        homePage.fillCity("Minsk");

        homePage.fillDateInDay("29");
        homePage.fillDateInMonth("12");
        homePage.fillDateInYear("2018");

        homePage.fillDateOutDay("30");
        homePage.fillDateOutMonth("12");
        homePage.fillDateOutYear("2018");

        homePage.search();
        homePage.searchSuccess();

        SearchPage searchPage = PageFactory.initElements(driver, SearchPage.class);

        String titleName = searchPage.getTitle();
        titleName = titleName.replaceAll("—.*", "");
        titleName = titleName.replaceAll("[^0-9]", "");
        int i = Integer.parseInt(titleName);
        Assert.assertTrue(i >= 3);
    }

    @Test
    public void searchFormFailure() {
        HomePage homePage = PageFactory.initElements(driver, HomePage.class);
        homePage.open();
        homePage.search();
        homePage.checkErrorRequiredField();
        String cityError = homePage.getErrorCity();
        Assert.assertEquals(cityError, "Ошибка:\n" + "Чтобы начать поиск, введите направление.");
    }

    @Test
    public void filterPrice() {
        HomePage homePage = PageFactory.initElements(driver, HomePage.class);

        homePage.open();

        homePage.fillCity("Minsk");

        homePage.fillDateInDay("29");
        homePage.fillDateInMonth("12");
        homePage.fillDateInYear("2018");

        homePage.fillDateOutDay("30");
        homePage.fillDateOutMonth("12");
        homePage.fillDateOutYear("2018");

        homePage.search();
        homePage.searchSuccess();

        SearchPage searchPage = PageFactory.initElements(driver, SearchPage.class);

        WebElement filterBox = driver.findElement(By.id("filter_price"));
        filterBox.findElement(By.cssSelector("[data-id=\"pri-3\"]")).click();

        searchPage.searchSuccess();
    }

    @Test
    public void searchDateOver() {
        HomePage homePage = PageFactory.initElements(driver, HomePage.class);

        homePage.open();

        homePage.fillCity("Minsk");

        homePage.fillDateInDay("11");
        homePage.fillDateInMonth("01");
        homePage.fillDateInYear("2019");

        homePage.fillDateOutDay("11");
        homePage.fillDateOutMonth("02");
        homePage.fillDateOutYear("2019");

        homePage.search();
        homePage.checkButtonSearch();

        String dateOverError = homePage.getErrorDate();
        Assert.assertEquals(dateOverError, "К сожалению, бронирование более чем на 30 ночей невозможно.");
    }
    @Test
    public void Login() {
        HomePage homePage = PageFactory.initElements(driver, HomePage.class);

        homePage.openLog();

        SignUpPage signUpPage = PageFactory.initElements(driver, SignUpPage.class);

        signUpPage.inputLogin("testforbooking@yandex.ru");
        signUpPage.submitForm();
        signUpPage.inputPassword("12345432");
        signUpPage.submitForm();

        signUpPage.checkSignUpSuccess();
    }

}




