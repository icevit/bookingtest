package booking.signUp;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class HomePage {
    private WebDriver driver;
    private WebDriverWait wait;

    public HomePage(WebDriver driver) {
        this.driver = driver;
        wait = new WebDriverWait(driver, 10);
    }

    @FindBy(id = "frm")
    private WebElement form;

    @FindBy(id = "ss")
    private WebElement cityForm;

    @FindBy(css = "input[name=\"checkin_monthday\"]")
    private WebElement dateDayIn;

    @FindBy(css = "input[name=\"checkin_month\"]")
    private WebElement dateMonthIn;

    @FindBy(css = "input[name=\"checkin_year\"]")
    private WebElement dateYearIn;

    @FindBy(css = "input[name=\"checkout_monthday\"]")
    private WebElement dateDayOut;

    @FindBy(css = "input[name=\"checkout_month\"]")
    private WebElement dateMonthOut;

    @FindBy(css = "input[name=\"checkout_year\"]")
    private WebElement dateYearOut;

    @FindBy(id = "destination__error")
    private WebElement errorCity;

    @FindBy(css = "div[class=\"fe_banner__message \"]")
    private WebElement errorDate;

    private By searchButtonLocator = By.cssSelector("button[type=\"submit\"]");
    private By loginNameRegister = By.id("login_name_register");
    private By searchForm = By.id("frm");
    private By formError = By.id("destination__error");
    private By loginName = By.id("loginname");


    public void openReg() {
        driver.get("https://account.booking.com/register/");
        wait.until(ExpectedConditions.visibilityOfElementLocated(loginNameRegister));
    }
    public void open() {
        driver.get("https://booking.com/");
    }
    public void search() {
        form.findElement(searchButtonLocator).click();
    }
    public void searchSuccess() {
        wait.until(ExpectedConditions.visibilityOfElementLocated(searchForm));
    }
    public void fillCity(String city) {
        cityForm.clear();
        cityForm.sendKeys(city);
    }
    public void fillDateInDay(String day) {
        dateDayIn.sendKeys(day);
    }
    public void fillDateInMonth(String month) {
        dateMonthIn.sendKeys(month);
    }
    public void fillDateInYear(String year) {
        dateYearIn.sendKeys(year);
    }
    public void fillDateOutDay(String day) {
        dateDayOut.sendKeys(day);
    }
    public void fillDateOutMonth(String month) {
        dateMonthOut.sendKeys(month);
    }
    public void fillDateOutYear(String year) {
        dateYearOut.sendKeys(year);
    }
    public void checkButtonSearch() {
        wait.until(ExpectedConditions.visibilityOfElementLocated(searchButtonLocator));
    }
    public void openLog() {
        driver.get("https://account.booking.com/");
        wait.until(ExpectedConditions.visibilityOfElementLocated(loginName));
    }

    //Errors
    public String getErrorCity() {
        return errorCity.getText();
    }
    public void checkErrorRequiredField() {
        wait.until(ExpectedConditions.visibilityOfElementLocated(formError));
    }

    public String getErrorDate() {
        return errorDate.getText();
    }

}
