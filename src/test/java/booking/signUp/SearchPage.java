package booking.signUp;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class SearchPage {
    private WebDriver driver;
    private WebDriverWait wait;

    public SearchPage(WebDriver driver) {

        this.driver = driver;
        wait = new WebDriverWait(driver, 10);
    }

    @FindBy(className = "sr_header ")
    private WebElement title;

    By titleLocator = By.cssSelector("h1");
    By searcResults = By.id("hotellist_inner");

    public String getTitle() {
        return title.findElement(titleLocator).getText();
    }

    public void searchSuccess() {
        wait.until(ExpectedConditions.visibilityOfElementLocated(searcResults));
    }
}
