package booking.signUp;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class SignUpPage {

    private WebDriver driver;
    private WebDriverWait wait;

    public SignUpPage(WebDriver driver) {
        this.driver = driver;
        wait = new WebDriverWait(driver, 10);
    }

    @FindBy(id = "login_name_register")
    private WebElement email;

    @FindBy(id = "password")
    private WebElement password;

    @FindBy(id = "confirmed_password")
    private WebElement passwordConfirm;

    @FindBy(css = "button[type=\"submit\"]")
    private WebElement submit;

    @FindBy(id = "loginname")
    private WebElement loginField;

    private By submitButtonLocator = By.cssSelector("button[type=\"submit\"]");
    private By passwordLocator = By.id("password");
    private By modalName = By.id("wl252-modal-name");
    private By formError = By.cssSelector("[class=\"bui-form__error\"]");

    public void inputEmail (String fieldEmail) {
        email.sendKeys(fieldEmail);
        driver.findElement(submitButtonLocator).click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(passwordLocator));
    }
    public void inputPassword(String pass) {
        wait.until(ExpectedConditions.visibilityOfElementLocated(passwordLocator));
        password.sendKeys(pass);
    }
    public void inputPasswordConfirm(String pass) {
        passwordConfirm.sendKeys(pass);
    }
    public void submitForm() {
        submit.click();
    }
    public void checkSignUpSuccess() {
        wait.until(ExpectedConditions.visibilityOfElementLocated(modalName));
    }

    public void inputLogin(String login) {
        loginField.sendKeys(login);
    }

    //Errors

    public void checkErrorRequiredField() {
        wait.until(ExpectedConditions.visibilityOfElementLocated(formError));
    }
    public String getErrorEmail() {
        WebElement emailAddressParrent = email.findElement(By.xpath(".."));
        return emailAddressParrent.findElement(By.cssSelector("div")).getText();
    }




}
